import sys
sys.path.append('./')

from sql.helper import SQLHelper
import csv
from user import User
from common.helper import Helper
from adhoc.extraRewards import ExtraRewards

class AchievementsHandler:

	def __init__(self, grantDate, finalRun):
		self.users = {}
		self.grantDate = grantDate
		self.finalRun = finalRun
		self.rewardDeducted = {}

	def addAchievements(self, userList, achievementId):
		print("Adding Achievements: {}: {}".format(achievementId + 1, len(userList)))
		for userId in userList:
			if userId not in self.users:
				self.users[userId] = User(userId)

			u = self.users[userId]
			u.addAchievements(achievementId)

	def loadAchievements(self, spark, startTime, endTime):
		print("loading achievements")
		deleted = self.cleanAchievements(spark)
		if deleted > 0:
			print("cleanAchievements: ", deleted)

		for i in range(6):
			query = None
			if i < 2:
				query = SQLHelper.GetQuery("achievement_" + str(i + 1)).format(startTime = startTime, endTime = endTime)
			else:
				query = SQLHelper.GetQuery("achievement_" + str(i + 1))
			userList = [x.user_id for x in spark.sql(query).rdd.collect()]
			self.addAchievements(userList, i)
			print("Added Achivement: {}: {}".format(i + 1, len(userList)))

	def loadReferrals(self, spark):
		print("Loading referrals")
		query = SQLHelper.GetQuery("referrals")
		data = spark.sql(query).rdd.collect()
		for entry in data:
			userId = entry["user_id"]
			if userId not in self.users:
				continue # no referrals to grant to this user

			self.users[userId].addReferrer(entry["referrer"])

	def getRewardDeducted(self, userId):
		if userId not in self.rewardDeducted:
			return 0
		return self.rewardDeducted[userId]

	def writeCSData(self):
		rewarded = 0
		with open('achievements_' + self.grantDate + ".csv", 'w') as f:
			csvwriter = csv.DictWriter(f, fieldnames = ['user_id', 'reward', 'trade_10', 'trade_50', 'trade_meta', 'top_1200', 'top_800', 'top_500', 'referrer', 'deducted'])

			csvwriter.writeheader()
			for userId, u in self.users.items():
				csData = u.getCSData()
				csData['deducted'] = self.getRewardDeducted(userId)
				csvwriter.writerow(csData)
				rewarded += u.getAchievementReward()
		print("Writing Achievement Data: {}: rewarded: {}".format(len(self.users), rewarded))

	def getReferralData(self):
		referralData = {}
		for userId, u in self.users.items():
			if u.referrer == None:
				continue
			if u.referrer not in referralData:
				referralData[u.referrer] = 0
			referralData[u.referrer] += u.getReferralReward()
		return referralData

	def writeRerralData(self):
		referralData = self.getReferralData()
		rewarded = 0
		with open('referrals_' + self.grantDate + '.csv', 'w') as f:
			csvwriter = csv.writer(f)

			csvwriter.writerow(['user_id', 'referral_reward', 'deducted'])
			for userId, reward in referralData.items():
				csvwriter.writerow([userId, reward, self.getRewardDeducted(userId)])
				rewarded += reward
		print("Written Referral Data: {}: rewarded: {}".format(len(referralData), rewarded))

	def getDistribution(self):
		distribution = {}

		def addReward(userId, reward):
			if userId not in distribution:
				distribution[userId] = 0

			distribution[userId] += reward

		for userId, u in self.users.items():
			addReward(userId, u.getAchievementReward())
			if u.referrer != None:
				addReward(u.referrer, u.getReferralReward())

		return distribution


	def writeDistribution(self):
		distribution = self.getDistribution()
		rewarded = 0
		totalDeducted = 0
		e = ExtraRewards()
		with open(Helper.GetFilePathInMyDir(__file__, 'distribution_' + self.grantDate + '.csv'), 'w') as f:
			csvwriter = csv.writer(f)

			csvwriter.writerow(['userId', 'quantity', 'tokenId'])

			for userId, galaReward in distribution.items():
				deducted = e.deductRewards(userId, galaReward)
				if deducted > 0:
					galaReward -= deducted
					self.rewardDeducted[userId] = deducted
					totalDeducted += deducted

				if galaReward > 0:
					rewarded += galaReward
					csvwriter.writerow([userId, galaReward * 1000 * 1000 * 100, "gala"])


		if self.finalRun:
			e.writeToFile()

		print("Written distribution: Rewarded: {}, Deducted: {}".format(rewarded, totalDeducted))
		return rewarded

	def getAchievementsTableName(self):
		return "hive_metastore.prd_slv_townstar_events.achievements"

	def cleanAchievements(self, spark):
		print("Deleting existing achievement entries: ", self.grantDate)
		query = "delete from {table} where time_rewarded = '{time_rewarded}'".format(table = self.getAchievementsTableName(), time_rewarded = self.grantDate)
		return spark.sql(query).rdd.collect()[0]["num_affected_rows"]

	def uploadAchievements(self, spark):
		print("uploadAchievements")
		values = []
		for u in self.users.values():
			values.extend(u.getRowsToInsert(self.grantDate))

		columns = ['user_id', 'id', 'name', 'details', 'reward_type', 'reward_value', 'time_rewarded', 'referrer', 'referrer_reward_type', 'referrer_reward_value']
		v = SQLHelper.insert(spark, self.getAchievementsTableName(), columns, values)
		print("uploaded achievements: ", v)


