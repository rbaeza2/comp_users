
from achievementsHandler import AchievementsHandler
import time

GrantDate = '2023-08-10'
StartDate = '2023-05-01'
EndDate = '2023-08-04'
FinalRun = True

if __name__ == "__main__":
	startTime = time.time()
	print("Evaluating Achivements")
	a = AchievementsHandler(GrantDate, FinalRun)
	a.loadAchievements(spark, StartDate, EndDate)
	a.loadReferrals(spark)

	a.writeDistribution() # must happen before anything else to find deducted rewards

	a.writeCSData()
	a.writeRerralData()
	a.uploadAchievements(spark)

	print("All Done: ", time.time() - startTime)



