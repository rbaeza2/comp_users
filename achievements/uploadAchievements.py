import csv
import math

import sys
sys.path.append('./')

from sql.helper import SQLHelper


def getInfoForAchievement(achievementId):
	rewards = [100, 200, 400]

	info = [[], [], []]
	info[0] = ['achievement_1', 'trade_10', 'Finish 10 trades in 1 game and connect Discord to your Gala account', 'GALA', rewards[0]]
	info[1] = ['achievement_2', 'trade_50', 'Finish 50 Trades in 1 game', 'GALA', rewards[1]]
	info[2] = ['achievement_3', 'trade_meta', 'Trade 10 of the meta craft in one game', 'GALA', rewards[2]]

	referrerInfo = []
	for i in range(3):
		referrerInfo.append(['GALA', math.ceil(rewards[i]/4)])
	return info[achievementId], referrerInfo[achievementId]


def getRowToInsert(achievementId, row, timeRewarded):
	achInfo, referrerInfo = getInfoForAchievement(achievementId)

	ans = [row['user_id']] + achInfo + [timeRewarded] 
	if row['referrer'] == '':
		ans += ['', '', 0]
	else:
		ans += [row['referrer']] + referrerInfo

	return ans


if __name__ == '__main__':
	timeRewarded = '2023-05-17 12:00:00'
	data = []
	rewards = [100, 200, 400]
	with open('achievements.csv', 'r') as f:
		reader = csv.DictReader(f)
		for row in reader:
			ach = [row['trade_10'], row['trade_50'], row['trade_meta']]
			totalReward = 0

			for i in range(3):
				if ach[i] != 'TRUE':
					continue

				totalReward += rewards[i]
				data.append(getRowToInsert(i, row, timeRewarded))

			if totalReward != int(row['reward']):
				print("Wrong: ", totalReward, row)

	for i in range(10):
		print(data[i])

	print(len(data))

	columns = ['user_id', 'id', 'name', 'details', 'reward_type', 'reward_value', 'time_rewarded', 'referrer', 'referrer_reward_type', 'referrer_reward_value']
	v = SQLHelper.insert(spark, "hive_metastore.prd_slv_townstar_events.achievements", columns, data)
	print(v)

