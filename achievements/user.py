import math

class User:

	def __init__(self, userId):
		self.userId = userId
		self.achievements = [False, False, False, False, False, False]
		self.rewards = [100, 200, 400, 800, 1600, 3200]
		self.referrer = None

	def addAchievements(self, achievementId):
		self.achievements[achievementId] = True

	def getAchievementReward(self):
		ans = 0
		for i in range(len(self.achievements)):
			ans += self.achievements[i] * self.rewards[i]
		return ans

	def addReferrer(self, referrer):
		self.referrer = referrer

	def getCSData(self):
		return {
			'user_id': self.userId,
			'reward': self.getAchievementReward(),
			'trade_10': self.achievements[0],
			'trade_50': self.achievements[1],
			'trade_meta': self.achievements[2],
			'top_1200': self.achievements[3],
			'top_800': self.achievements[4],
			'top_500': self.achievements[5],
			'referrer': self.referrer
		}

	def getReferralReward(self):
		return math.ceil(self.getAchievementReward() / 4)

	def getRewardForAchievement(self, achievementId):
		return self.rewards[achievementId]

	def getAchievementInfo(self, achievementId):
		info = [[], [], [], [], [], []]
		info[0] = ['achievement_1', 'trade_10', 'Finish 10 trades in 1 game and connect Discord to your Gala account', 'GALA', self.rewards[0]]
		info[1] = ['achievement_2', 'trade_50', 'Finish 50 Trades in 1 game', 'GALA', self.rewards[1]]
		info[2] = ['achievement_3', 'trade_meta', 'Trade 10 of the meta craft in one game', 'GALA', self.rewards[2]]
		info[3] = ['achievement_4', 'top_1200', 'Reach top 1200 in a competition', 'GALA', self.rewards[3]]
		info[4] = ['achievement_5', 'top_800', 'Reach top 800 in a competition and top 1200 in 7 competitions', 'GALA', self.rewards[4]]
		info[5] = ['achievement_6', 'top_500', 'Reach top 500 in a competition, 800 in 3 and 1200 in 10', 'GALA', self.rewards[5]]

		return info[achievementId]


	def getRowForAchievement(self, achievementId, timeRewarded):
		aInfo = self.getAchievementInfo(achievementId)

		ans = [self.userId] + aInfo + [timeRewarded] 
		if self.referrer == None:
			ans += ['', '', 0]
		else:
			ans += [self.referrer, 'GALA', math.ceil(self.getRewardForAchievement(achievementId)/4)]

		return ans

	def getRowsToInsert(self, timeRewarded):
		rows = []
		for i in range(len(self.achievements)):
			if self.achievements[i]:
				rows.append(self.getRowForAchievement(i, timeRewarded))
		return rows