import csv
import sys
import os
from common.helper import Helper


class ExtraRewards:

	def __init__(self):
		self.fileName = 'extra_rewards.csv'
		self.keys = ['user_id', 'reward']
		self.rewards = {}
		with open(Helper.GetFilePathInMyDir(__file__, self.fileName), 'r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				self.rewards[row['user_id']] = int(row['reward'])


	def deductRewards(self, userId, reward):
		if userId not in self.rewards:
			return 0

		deducted = min(self.rewards[userId], reward)
		self.rewards[userId] -= deducted
		return deducted


	def writeToFile(self):
		with open(Helper.GetFilePathInMyDir(__file__, self.fileName), 'w') as f:
			writer = csv.writer(f)
			writer.writerow(self.keys)
			for userId, reward in self.rewards.items():
				writer.writerow([userId, reward])
