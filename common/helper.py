import os

class Helper:
	def GetFilePathInMyDir(callingFilePath, fileName):
		location = os.path.realpath(
		    os.path.join(os.getcwd(), os.path.dirname(callingFilePath)))
		return os.path.join(location, fileName)

	def CreateDirInMyDir(callingFilePath, subPath):
		path = Helper.GetFilePathInMyDir(callingFilePath, subPath)
		os.makedirs(path, exist_ok=True)
		return path

	def GetPathInMyDir(callingFilePath, subPath):
		path = Helper.GetFilePathInMyDir(callingFilePath, subPath)
		return path

	def CAssert(left, right):
		assert left == right, "{} != {}".format(left, right)
