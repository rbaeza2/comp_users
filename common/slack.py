import requests

Webhooks = {"analytics": "https://hooks.slack.com/services/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"}

def post(channel, data):
	hook = Webhooks[channel]
	if channel not in Webhooks:
		hook = Webhooks["analytics"]

	requests.post(hook, json={
			'text': data
		})
