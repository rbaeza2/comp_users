import sys
sys.path.append('./')
from common.helper import Helper
import json

class CompConfig:

	def __init__(self):
		with open(Helper.GetFilePathInMyDir(__file__, "config.json"), 'r') as f:
			self.config = json.load(f)

	# uses comp id, although it can be anything unique
	def getCompName(self):
		return self.config["comp_id"]

	def getCompId(self):
		return self.config["comp_id"]

	def getMaxRank(self):
		return self.config['max_rank']

	def getStartTime(self):
		return self.config['start_time']

	def getEndTime(self):
		return self.config['end_time']

	def getLbdEntriesPerCall(self):
		return self.config['lbd_entries_per_call']

	def getLbdUserId(self):
		return self.config['lbd_user_id']

	def getParticipationReward(self):
		return self.config["participation_reward"]

	def getTokenForNFTReward(self, nftReward):
		return self.config["nft_reward"][self.getCompName()][nftReward]

	def getCompRewardForRank(self, rank):
		for rankConf in self.config["rewards"]:
			if rank >= rankConf["min"] and rank <= rankConf["max"]:
				return rankConf["gala"], rankConf["nft"]
		print("Wrong: ", rank)

	def getMetaItem(self):
		return self.config['meta_item']


	def shouldRewardNFT(self):
		return bool(self.config['rewards_nft'])