import sys
sys.path.append('../')

from sql.helper import SQLHelper
import leaderboard
from common import slack
from config import CompConfig
import math

# returns the count of deleted entries
def deleteExistingLBData(l, compConf):
	print("Deleting existing comp entries: ", compConf.getCompName())
	query = "delete from {table} where comp_name = '{comp_name}'".format(table = l.getLBTable(), comp_name = compConf.getCompName())
	print(query)
	return spark.sql(query).rdd.collect()[0]["num_affected_rows"]


def insertLBData(l):
	print("Inserting Data into Leaderboard Table")
	table = l.getLBTable()
	columns = l.getLBTableColumns()
	values = l.getLBTableValues()

	df = spark.createDataFrame(values, schema = columns)
	return df.write.insertInto(table)

if __name__ == "__main__":
	compConf = CompConfig()

	l = leaderboard.Leaderboard(compConf.getLbdUserId(), compConf.getCompId(), compConf.getCompName(), compConf.getStartTime(), compConf.getEndTime())

	ranksToFetch = math.floor(compConf.getMaxRank() * 10)
	maxIterations = math.ceil(2 * ranksToFetch / compConf.getLbdEntriesPerCall()) # keep it on the high side to be exhaustive
	entries = l.pullLeaderboard(ranksToFetch, maxIterations)
	print("\n\nPulled Leaderboard: ", len(l.entries))


	deletedCount = deleteExistingLBData(l, compConf)
	if deletedCount > 0:
		print("Deleted Existing Entries: ", deletedCount)

	error = insertLBData(l)
	if error != None:
		print("Inserting: error: ", error)

	print("\n\nFetched and uploaded leaderboard: {}\nTotal Entries: {}\n".format(compConf.getCompName(), len(l.entries)))
	slack.post("analytics", "Fetched and Uploaded Leaderboard : {}. Total Players: {}".format(compConf.getCompName(), len(l.entries)))

