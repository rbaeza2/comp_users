
from rewardHandler import RewardHandler
from config import CompConfig
from common import slack

FinalRun = False


if __name__ == '__main__':
	print("Calculating rewards")
	compConf = CompConfig()
 
	handler = RewardHandler(FinalRun)

	handler.fetchUserInfo(spark)
	handler.removeDuplicates()
	handler.finalizeAndUploadRankings(spark)
	galaRewarded = handler.calcRewards(spark, False)
	handler.writeDistribution() # must happen before writing any other info
	handler.writeRankings()
	handler.writeCSData()


	print("\n\nRewards Generated: {}\nTotal Gala Rewarded: {}\n".format(compConf.getCompName(), galaRewarded))
	slack.post("analytics", "Rewards Generated: {}\nTotal Gala Rewarded: {}\n".format(compConf.getCompName(), galaRewarded))
