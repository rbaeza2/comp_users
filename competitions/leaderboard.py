import urllib.request
import json
import time


class LeaderboardEntry:

	def __init__(self, entry):
		self.userId = entry["userId"]
		self.name = entry["name"]
		self.stars = entry["stars"]
		self.rank = entry["rank"]
		self.avatarUrl = entry["avatarURL"]
		self.townName = entry["townName"]
		self.serverId = entry["serverId"]

	def getSQLData(self):
		return [self.userId, self.name, self.stars, self.rank, self.townName]

class Leaderboard:

	LBTable = "hive_metastore.prd_slv_townstar_events.leaderboard"

	def __init__(self, user_id, compId, compName, startTime, endTime):
		self.userId = user_id
		self.compId = compId

		self.compName = compName
		self.startTime = startTime
		self.endTime = endTime

		self.entries = []
		self.RankIncrement = 20
		self.maxRank = 0

	def pullLeaderboard(self, maxRank, maxIterations):
		print("Pulling Leaderboard: ", self.compId)

		rank = 0 # rank that we have
		iterations = 0

		while rank < maxRank and iterations < maxIterations:
			rank += self.RankIncrement # api returns RankIncrement entries before the rank, including it
			iterations += 1

			success, entries = self.getLeaderboardEntries(rank)
			if success == False:
				self.entries = [] # if failed, let's not use the fetched data
				break
			elif len(entries) == 0: # nothing left to fetch
				break

			for e in entries:
				self.entries.append(e)
				rank = max(rank, e.rank)
				self.maxRank = max(rank, self.maxRank)

			time.sleep(1) # wait so as not to bombard prod

		if iterations == maxIterations:
			print("pullLeaderboard: Exhausted iterations")

		return self.entries


	def fetchUrl(self, url):
		req = urllib.request.Request(url)

		try:
			with urllib.request.urlopen(req) as rawResp:
			   resp = rawResp.read()
			   return json.loads(resp)
		except Exception as error:
			print("leaderboard: fetchUrl: failed: ",  error)

		return {}

	def getUrlForRank(self, rank):
		url = "https://prod-gala-tsf-api.gala.games/admin/competitions/{competition}/user/{user_id}/leaderboard/{rank}"
		return url.format(competition = self.compId, user_id = self.userId, rank = rank)


	def getLeaderboardEntries(self, rank):
		print("getLeaderboardEntries: ", rank)
		url = self.getUrlForRank(rank)
		response = self.fetchUrl(url)

		if "success" not in response or response["success"] == False:
			print("getLeaderboardEntries: response: failed: ", response)
			return False, []

		rawEntries = response["response"]["leaderboard"]
		entries = [LeaderboardEntry(x) for x in rawEntries]
		return True, entries

	def getLBTable(self):
		return Leaderboard.LBTable

	def getLBTableColumns(self):
		return ["user_id", "name", "stars", "rank", "town_name", "comp_name", "start_time", "end_time", "comp_id"]

	def getLBTableValues(self):
		commonData = [self.compName, self.startTime, self.endTime, self.compId]
		return [x.getSQLData() + commonData for x in self.entries]



