import sys
sys.path.append('./')
from sql.helper import SQLHelper
from common.helper import Helper
import csv
import json
import math
from adhoc.extraRewards import ExtraRewards
from config import CompConfig

class User:
	def __init__(self, info):
		self.userId = info["user_id"]
		self.rank = info["rank"]
		self.name = info["name"]
		self.stars = info["stars"]
		self.town = info["town_name"]
		self.compReward = 0
		self.loyaltyReward = 0
		self.nftReward = ""
		self.mayhems = [False, False]
		self.deducted = 0
		self.finalRank = -1

	def getFinalSQLData(self):
		return [self.userId, self.finalRank, self.name, self.town, self.stars]

	def __repr__(self):
		return str(self.getFinalSQLData())

	def grantCompReward(self, reward, nft):
		self.compReward += reward
		self.nftReward = nft

	def grantParticipationReward(self, nft):
		if self.nftReward != "":
			print("grantParticipationReward: Already granted: ", self.userId)
			return 0

		self.nftReward = nft
		return 1

	def grantLoyaltyReward(self, multiplier):
		self.loyaltyReward += math.floor(self.compReward * multiplier)
		return self.loyaltyReward

	def getTotalGalaRewards(self):
		return self.compReward + self.loyaltyReward

	def getNFTReward(self):
		return self.nftReward
	
	def setDeducted(self, deducted):
		self.deducted = deducted

	def getCSData(self):
		return [self.userId, self.finalRank, self.name, self.stars, self.town, self.compReward, self.nftReward, 
		 self.getTotalGalaRewards() - self.deducted, self.rank, self.deducted]


class RewardHandler:

	# if final run is set to true, it'll deduct rewards based on the extraRewards.json
	def __init__(self, finalRun):
		self.users = {}
		self.finalRun = finalRun

		self.rankingUsers = {}
		self.compConf = CompConfig()
		self.compName = self.compConf.getCompName()

	def fetchUserInfo(self, spark):
		print("fetchUserInfo")
		query = SQLHelper.GetQuery('leaderboard')
		query = query.format(compName = self.compName)
		for info in spark.sql(query).rdd.collect():
			self.users[info["user_id"]] = User(info)
		print("Fetched Users: ", len(self.users))
		return len(self.users)

	def getFinalRankingsColumns(self):
		return ["user_id", "final_rank", "name", "town", "stars", "comp_name", "comp_id"]

	def getRankingsTableName(self):
		return "hive_metastore.prd_slv_townstar_events.rankings"

	def getCompFolder(self):
		return Helper.GetPathInMyDir(__file__, "store/" + self.compName)

	def removeDuplicates(self):
		print("Removing duplicates")
		duplicates = set()
		compPath = self.getCompFolder()
		
		with open(compPath + '/duplicates.csv', 'r') as f:
			reader = csv.DictReader(f)
			for row in reader:
				duplicates.add(row['user_id'])

		newUsers = {}
		for userId, u in self.users.items():
			if userId not in duplicates:
				newUsers[userId] = u
		print("removeDuplicates: done: removed: ", len(self.users) - len(newUsers))
		self.rankingUsers = newUsers

	def cleanRankings(self, spark):
		print("Deleting existing comp entries: ", self.compName)
		query = "delete from {table} where comp_name = '{comp_name}'".format(table = self.getRankingsTableName(), comp_name = self.compName)
		return spark.sql(query).rdd.collect()[0]["num_affected_rows"]

	def uploadRankings(self, spark):
		print("uploadRankings")
		deleted = self.cleanRankings(spark)
		if deleted > 0:
			print("cleanRankings: ", deleted)

		values = [u.getFinalSQLData() + [self.compName, "20230718_ChandelierEarringsBracelets"] for u in self.rankingUsers.values()]
		v = SQLHelper.insert(spark, self.getRankingsTableName(), self.getFinalRankingsColumns(), values)
		print("uploaded rankings: ", v)


	def finalizeAndUploadRankings(self, spark):
		print("finalizeAndUploadRankings")
		maxRank = self.compConf.getMaxRank()

		userList = [u for userId, u in self.rankingUsers.items()]
		userList = sorted(userList, key = lambda x: x.rank)

		userList = userList[:maxRank]
		self.rankingUsers = {}
		for i in range(len(userList)):
			userList[i].finalRank = i + 1
			self.rankingUsers[userList[i].userId] = userList[i]

		print("Finalized rankings: ", len(self.rankingUsers))
		self.uploadRankings(spark)


	def getMayhemPlayers(self, spark, year):
		query = SQLHelper.GetQuery("loyalty").format(year = year, compName = self.compName)
		return set(x["user_id"] for x in spark.sql(query).rdd.collect())

	def getMayhem2021Players(self, spark):
		query = SQLHelper.GetQuery("loyalty_2021").format(compName = self.compName)
		return set(x["user_id"] for x in spark.sql(query).rdd.collect())

	def addLoyaltyRewards(self, spark):
		print("adding loyalty rewards")
		firstYear = self.getMayhem2021Players(spark)
		secondYear = self.getMayhemPlayers(spark, 2022)

		rewarded = 0
		for userId, u in self.rankingUsers.items():
			if userId in firstYear and userId in secondYear:
				rewarded += u.grantLoyaltyReward(0.6)
			elif userId in firstYear or userId in secondYear:
				rewarded += u.grantLoyaltyReward(0.25)

			if userId in firstYear:
				u.mayhems[0] = True
			if userId in secondYear:
				u.mayhems[1] = True

		print("Granted Loyalty Rewards: ", rewarded)
		return rewarded

	def addCompRewards(self):
		print("adding comp rewards")

		rewarded = 0
		for userId, u in self.rankingUsers.items():
			gala, nft = self.compConf.getCompRewardForRank(u.finalRank)
			rewarded += gala
			u.grantCompReward(gala, nft)

		print("Granted Comp rewars: ", rewarded)
		return rewarded

	def addParticipationRewards(self, spark):
		print("addParticipationRewards")
		query = SQLHelper.GetQuery('participation_reward_vanilla')
		if self.compConf.getMetaItem() != "":
			query = SQLHelper.GetQuery('participation_reward').format(meta_item = self.compConf.getMetaItem())

		query = query.format(comp_id = self.compConf.getCompId(), comp_name = self.compName, 
			start_time = self.compConf.getStartTime(), end_time = self.compConf.getEndTime())
		granted = 0
		for info in spark.sql(query).rdd.collect():
			if info['user_id'] not in self.users:
				print("addParticipationRewards: Missing User: ", info['user_id'])
				continue
			granted += self.users[info['user_id']].grantParticipationReward(self.compConf.getParticipationReward())

		print("Granted Participation Rewards: ", granted)
		return granted

	def calcRewards(self, spark, loyalty = False):
		print("Calculating rewards")
		rewarded = 0

		rewarded += self.addCompRewards()

		if loyalty:
			rewarded += self.addLoyaltyRewards(spark)

		self.addParticipationRewards(spark)
		return rewarded

	def writeDistribution(self):
		rewarded = 0
		totalDeducted = 0
		e = ExtraRewards()

		with open(self.getCompFolder() + '/distribution.csv', 'w') as f:
			csvwriter = csv.writer(f)
			csvwriter.writerow(['userId', 'quantity', 'tokenId'])

			for userId, u in self.rankingUsers.items():
				galaReward = u.getTotalGalaRewards()
				deducted = e.deductRewards(userId, galaReward)
				if not self.finalRun:
					deducted = 0

				if deducted > 0:
					galaReward -= deducted
					u.setDeducted(deducted)
					totalDeducted += deducted

				if galaReward > 0:
					csvwriter.writerow([userId, galaReward * 100 * 1000 * 1000, "gala"])
					rewarded += galaReward

			for userId, u in self.users.items():
				if u.getNFTReward() != "" and self.compConf.shouldRewardNFT():
					csvwriter.writerow([userId, 1, self.compConf.getTokenForNFTReward(u.getNFTReward())])

		if self.finalRun:
			e.writeToFile()

		print("Written distribution: Rewarded: {}, Deducted: {}".format(rewarded, totalDeducted))

	def writeRankings(self):
		with open(self.getCompFolder() + "/rankings.csv", 'w') as f:
			csvwriter = csv.writer(f)

			csvwriter.writerow(['rank', 'stars', 'name', 'town', 'Reward - Total', 'Reward - Comp', 'Reward - Loyalty', 'Reward - Deducted'])
			for userId, u in self.rankingUsers.items():
				csvwriter.writerow([u.finalRank, u.stars, u.name, u.town, u.getTotalGalaRewards() - u.deducted, u.compReward, u.loyaltyReward, u.deducted])
		print("Written rankings")

	def writeCSData(self):
		with open(self.getCompFolder() + "/cs.csv", 'w') as f:
			csvwriter = csv.writer(f)

			csvwriter.writerow(['User Id', 'Rank', 'Name', 'Stars', 'Town', 'Comp Reward', 'NFT Reward'
				, 'Total Rewards (minus Deducted)', 'Original Rank', 'Rewards Deducted'])
			for userId, u in self.rankingUsers.items():
				csvwriter.writerow(u.getCSData())
			for userId, u in self.users.items():
				if userId not in self.rankingUsers and u.getNFTReward() != "":
					csvwriter.writerow(u.getCSData())


		print("Written CS Data")

