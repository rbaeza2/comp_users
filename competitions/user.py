class User:
	def __init__(self, info):
		self.userId = info['user_id']
		if 'first_name' in info and info['first_name'] != None:
			self.name = info['first_name'] + ' ' + info['last_name']
		else:
			self.name = ""
			print('User: Missing Name: ', self.userId)
		self.email = info['email'] if 'email' in info and info['email'] != None else ""
		self.created = info['created']
		self.parseIps(info)
		self.ipDuplicates = set()
		self.duplicates = {}
		self.rank = 0
		self.town = ""
		self.revenue = 0

	def __repr__(self):
		return str([self.userId, self.name, self.email, self.created, self.ips])

	def setCompData(self, info):
		self.rank = info["rank"]
		self.town = info["town_name"]

	def parseIps(self, info):
		self.ips = set(info['verified_ips'])
		if info['un_verified_ip'] != None:
			self.ips.add(info["un_verified_ip"])
		if info['user_ip_address'] != None:
			self.ips.add(info["user_ip_address"])

	def get(self, key):
		if key == 'name':
			return self.name
		if key == 'email':
			return self.email
		if key == 'ips':
			return self.ips
		if key == 'created':
			return self.created
		if key == 'revenue':
			return self.revenue

		if key == 'town':
			return self.town

		return None

	def addRevenue(self, rev):
		self.revenue = rev

	def markIPDuplicates(self, oUser):
		if oUser.userId == self.userId:
			return False

		for ip in self.ips:
			if ip in oUser.ips:
				self.ipDuplicates.add(oUser.userId)
				return True

		return False

	def addDuplicate(self, key, oId):
		if oId not in self.duplicates:
			self.duplicates[oId] = []
		self.duplicates[oId].append(key)

	def getCSData(self):
		return [self.userId, self.rank, self.name, self.email, self.town, self.created, self.ips]