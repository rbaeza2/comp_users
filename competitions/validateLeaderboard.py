from validator import Validator
from user import User
from common.helper import Helper
from common import slack
from config import CompConfig
import math


class Test:

	def __init__(self):
		self.ip1 = '1.1.1.1'
		self.ip2 = '2.2.2.2'
		self.ip3 = '3.3.3.3'
		data = {
			'user1': {'user_id': 'user1', 'first_name': 'User', 'last_name': 'One', 'email': 'user1@gmail.com', 'created': '2023-05-16',
			'un_verified_ip': self.ip1, 'verified_ips': [self.ip2], 'user_ip_address': self.ip3},
			'user2': {'user_id': 'user2', 'first_name': 'User', 'last_name': 'OneT', 'email': 'user2@gmail.com', 'created': '2023-05-16',
			'un_verified_ip': '', 'verified_ips': [self.ip1], 'user_ip_address': None},
			'user3': {'user_id': 'user3', 'first_name': 'Third', 'last_name': 'User', 'email': 'user3@gmail.com', 'created': '2023-05-16',
			'un_verified_ip': self.ip2, 'verified_ips': [], 'user_ip_address': None},
		}

		self.v = Validator("Test Comp")
		for userId, info in data.items():
			self.v.users[userId] = User(info)

	def run(self):
		print('Running tests')
		Helper.CAssert(self.v.users['user1'].ips, set([self.ip1, self.ip2, self.ip3]))

		self.v.markIPDuplicates()
		Helper.CAssert(self.v.users['user1'].ipDuplicates, set(['user2', 'user3']))

		self.v.markDuplicates()
		Helper.CAssert(self.v.users['user1'].duplicates, {'user2': ['name', 'email'], 'user3': ['email']})

		self.v.writeInvestigativeData(100)

		print('Tests: Passed')



if __name__ == "__main__":

	t = Test()
	t.run()

	compConf = CompConfig()
	print("Starting Process: ", compConf.getCompName())
	v = Validator(compConf.getCompName())
	v.fetchUserInfo(spark)
	v.markIPDuplicates()
	v.markDuplicates()

	ranksToInvestigate = math.floor(compConf.getMaxRank() * 1.25) # investigate 25% more since somep players will get removed
	duplicateCounts = v.writeInvestigativeData(ranksToInvestigate)
	for key, v in duplicateCounts.items():
		print("Rank: {}, duplicates: {}".format(key, len(v)))

	slack.post("analytics", "User Validation done.\nDuplicates Found: {}.\nPlease confirm duplicates.".format(len(duplicateCounts[1200])))
	print("All done!!")


