import sys
sys.path.append('./')
import csv
import json

from sql.helper import SQLHelper
from competitions.user import User
from common.helper import Helper

EditDistForName = 1
EditDistForEmail = 3
RevenueCheckForDuplicates = 100

class Validator:

	def __init__(self, compName):
		self.compName = compName
		self.users = {}
		self.whitelist = self.loadWhitelist()

	def loadWhitelist(self):
		with open(Helper.GetFilePathInMyDir(__file__, "whitelist.json"), 'r') as f:
			return json.load(f)

	def fetchUserInfo(self, spark):
		print("Fetching User Info")
		query = SQLHelper.GetQuery("leaderboard_user_info")
		query = query.format(compName = self.compName)
		userList = [User(x) for x in spark.sql(query).rdd.collect()]
		for u in userList:
			self.users[u.userId] = u

		query = SQLHelper.GetQuery('leaderboard')
		query = query.format(compName = self.compName)
		for info in spark.sql(query).rdd.collect():
			self.users[info["user_id"]].setCompData(info)


		query = SQLHelper.GetQuery("leaderboard_revenue").format(compName = self.compName)
		for info in spark.sql(query).rdd.collect():
			self.users[info["user_id"]].addRevenue(info["revenue"])
		print("Fetched Info: ", len(self.users))



	def markIPDuplicates(self):
		print("Marking IP duplicates")
		for userId, u in self.users.items():
			for oId, oUser in self.users.items():
				u.markIPDuplicates(oUser)

	def checkKey(self, key, editDistance):
		print("checking: ", key)
		count = 0
		for userId, u in self.users.items():
			for oId in u.ipDuplicates:
				if userId >= oId:
					continue

				oUser = self.users[oId]

				if key == 'name' and u.get(key) == 'Gamer Gamer':
					continue

				if areSimilar(u.get(key), oUser.get(key), editDistance):
					u.addDuplicate(key, oId)
					oUser.addDuplicate(key, userId)
					count += 1

		return count


	def markDuplicates(self):
		print("Marking duplicates")
		nameDuplicates = self.checkKey('name', EditDistForName)
		emailDuplicates = self.checkKey('email', EditDistForEmail)
		print("Found duplicates - name: {}, email: {}".format(nameDuplicates, emailDuplicates))

	def specialTreatment(self, userId):
		if userId in self.whitelist:
			return True

		if userId in self.users and self.users[userId].get('revenue') >= RevenueCheckForDuplicates:
			return True
		return False

	def createCompFolder(self):
		return Helper.CreateDirInMyDir(__file__, "store/" + self.compName)

	def writeInvestigativeData(self, rankLimit):
		print("Writing investigative Data")
		compPath = self.createCompFolder()
		keys = ['user_id', 'rank', 'name', 'email', 'town', 'created', 'ips', 'duplicate_name', 'duplicate_email']
		count = 0
		rankCounts = {
			1200: set(),
			2500: set(),
			5000: set(),
			10000: set(),
			100000: set()
		}
		rankCounts[rankLimit] = set()

		with open(compPath + '/duplicates_' + str(rankLimit) + '.csv', 'w') as f:
			csvwriter = csv.writer(f)
			csvwriter.writerow(keys)

			for userId, u in self.users.items():
				uRow = u.getCSData()

				for oId in u.duplicates:
					if userId >= oId:
						continue

					oUser = self.users[oId]

					count += 1
					for r in rankCounts:
						if u.rank <= r:
							rankCounts[r].add(userId)
						if oUser.rank <= r:
							rankCounts[r].add(oId)

					if self.specialTreatment(u.userId) or self.specialTreatment(oUser.userId):
						continue

					if u.rank > rankLimit and oUser.rank > rankLimit:
						continue

					oRow = oUser.getCSData()

					duplicateName = 'name' in u.duplicates[oId]
					duplicateEmail = 'email' in u.duplicates[oId]

					csvwriter.writerow(uRow + [duplicateName, duplicateEmail])
					csvwriter.writerow(oRow + [duplicateName, duplicateEmail])
					csvwriter.writerow([])
					csvwriter.writerow([])

		return rankCounts


def areSimilar(lVal, rVal, editDistanceCheck):
	if editDistanceCheck == 0:
		return lVal == rVal
	dist = levenshteinDistance(lVal, rVal)
	return dist <= editDistanceCheck


def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]
