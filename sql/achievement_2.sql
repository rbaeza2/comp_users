WITH loads AS
  ( SELECT DISTINCT user_id
   FROM prd.game_telemetry.brz_landing
   WHERE game = 'town-star-forever'
     AND event_type IN ('session_start',
                        'trade_complete',
                        'in_game_spend')
     AND to_date(TIMESTAMP) >='2023-05-01'),
     purchasers AS
  (SELECT *
   FROM hive_metastore.gold_bi.store_revenue
   WHERE status = 'fulfilled'
     AND total_price_usd > 0
     AND game NOT IN ('bridge',
                      'swap')),
     no_bots AS
  ( SELECT DISTINCT a.user_id AS user_id
   FROM loads a
   LEFT JOIN dev.anna_temp.mayhem_selected_multiacc b ON a.user_id = b.user_id
   LEFT JOIN purchasers c ON a.user_id = c.userid
   AND b.user_id IS NOT NULL
   WHERE b.user_id IS NULL
     OR c.userid IS NOT NULL),
     townTrades AS (WITH towns AS (WITH town_creation AS
                                     (SELECT user_id,
                                             source_timestamp AS creation
                                      FROM prd.game_telemetry.brz_landing
                                      WHERE game = 'town-star-forever'
                                        AND event_type = 'create_board'
                                        AND source_timestamp >= '{startTime}'
                                        AND source_timestamp <= '{endTime}')
                                   SELECT user_id,
                                          CASE WHEN startTime IS NULL THEN '{startTime}' ELSE startTime END AS startTime,
                                                                                                               min(CASE WHEN endTime IS NULL THEN '{endTime}' ELSE endTime END) AS endTime
                                   FROM
                                     (SELECT CASE WHEN t1.user_id IS NULL THEN t2.user_id ELSE t1.user_id END AS user_id,
                                                                                                                 t1.creation AS startTime,
                                                                                                                 t2.creation AS endTime
                                      FROM town_creation t1
                                      FULL JOIN town_creation t2 ON t1.user_id = t2.user_id
                                      AND t1.creation < t2.creation)
                                   GROUP BY user_id,
                                            startTime
                                   ORDER BY user_id,
                                            startTime),
                         trades AS
                      (SELECT user_id,
                              source_timestamp AS tradeTime
                       FROM prd.game_telemetry.brz_landing
                       WHERE game = 'town-star-forever'
                         AND event_type = 'trade_complete'
                         AND source_timestamp >= '{startTime}'
                         AND source_timestamp <= '{endTime}')
                    SELECT towns.user_id,
                           towns.startTime,
                           towns.endTime,
                           count(*) AS totalTrades
                    FROM towns
                    JOIN trades ON towns.user_id = trades.user_id
                    AND trades.tradeTime BETWEEN towns.startTime AND towns.endTime
                    GROUP BY towns.user_id,
                             towns.startTime,
                             towns.endTime
                    ORDER BY towns.user_id,
                             towns.startTime)
SELECT DISTINCT user_id
FROM townTrades
WHERE user_id NOT IN
    (SELECT user_id
     FROM hive_metastore.prd_slv_townstar_events.achievements
     WHERE id = 'achievement_2')
  AND user_id IN
    (SELECT user_id
     FROM no_bots)
  AND totalTrades >= 50;

