WITH loads AS
  ( SELECT DISTINCT user_id
   FROM prd.game_telemetry.brz_landing
   WHERE game = 'town-star-forever'
     AND event_type IN ('session_start',
                        'trade_complete',
                        'in_game_spend')
     AND to_date(TIMESTAMP) >='2023-05-01'),
     purchasers AS
  (SELECT *
   FROM hive_metastore.gold_bi.store_revenue
   WHERE status = 'fulfilled'
     AND total_price_usd > 0
     AND game NOT IN ('bridge',
                      'swap')),
     no_bots AS
  ( SELECT DISTINCT a.user_id AS user_id
   FROM loads a
   LEFT JOIN dev.anna_temp.mayhem_selected_multiacc b ON a.user_id = b.user_id
   LEFT JOIN purchasers c ON a.user_id = c.userid
   AND b.user_id IS NOT NULL
   WHERE b.user_id IS NULL
     OR c.userid IS NOT NULL),
     user_details AS
  ( SELECT a.user_id,
           b.referred_by,
           b.discord_id
   FROM no_bots a
   LEFT JOIN prd.gala_blockchaingamepartners.slv_users b ON a.user_id = b.user_id),
     meta_trades AS
  ( SELECT user_id,
           COUNT(event_type) AS meta_trades,
           SUM(get_json_object(event_detail, '$.quantity')) AS qty
   FROM prd.game_telemetry.brz_landing
   WHERE game = 'town-star-forever'
     AND ( (get_json_object(event_detail, '$.item') = 'shrimp_pizza'
            AND source_timestamp >= '2023-05-09 17:00:00'
            AND source_timestamp <= '2023-05-13 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'cheese_pizza'
              AND source_timestamp >= '2023-05-16 17:00:00'
              AND source_timestamp <= '2023-05-20 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'risotto'
              AND source_timestamp >= '2023-05-23 17:00:00'
              AND source_timestamp <= '2023-05-27 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'lasagna'
              AND source_timestamp >= '2023-05-30 17:00:00'
              AND source_timestamp <= '2023-06-03 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'uniforms'
              AND source_timestamp >= '2023-06-06 17:00:00'
              AND source_timestamp <= '2023-06-09 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'cabernet_sauvignon'
              AND source_timestamp >= '2023-06-14 17:00:00'
              AND source_timestamp <= '2023-06-17 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'chandelier_earrings'
              AND source_timestamp >= '2023-07-04 17:00:00'
              AND source_timestamp <= '2023-07-07 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'bracelet'
              AND source_timestamp >= '2023-07-11 17:00:00'
              AND source_timestamp <= '2023-07-14 17:00:00')
          OR (get_json_object(event_detail, '$.item') IN ('bracelet',
                                                          'chandelier_earrings')
              AND source_timestamp >= '2023-07-19 17:00:00'
              AND source_timestamp <= '2023-07-21 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'fancy_cake'
              AND source_timestamp >= '2023-07-25 17:00:00'
              AND source_timestamp <= '2023-07-28 17:00:00')
          OR (get_json_object(event_detail, '$.item') = 'decorated_cake'
              AND source_timestamp >= '2023-08-01 17:00:00'
              AND source_timestamp <= '2023-08-04 17:00:00') )
GROUP BY user_id ), combined AS
  ( SELECT a.user_id,
           b.referred_by,
           b.discord_id,
           d.qty,
           CASE
               WHEN qty >= 10 THEN 700
           END AS gala_3,
           CASE
               WHEN qty >= 10
                    AND referred_by != '' THEN 175
           END AS gala_3_r
   FROM no_bots a
   LEFT JOIN user_details b ON a.user_id = b.user_id
   LEFT JOIN meta_trades d ON a.user_id = d.user_id)
SELECT DISTINCT user_id
FROM combined
WHERE gala_3 = 700
  AND user_id NOT IN
    (SELECT DISTINCT user_id
     FROM hive_metastore.prd_slv_townstar_events.achievements
     WHERE id = 'achievement_3');