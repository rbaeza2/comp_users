WITH loads AS (
  SELECT DISTINCT user_id
  FROM prd.game_telemetry.brz_landing
  WHERE game = 'town-star-forever'
  AND event_type IN ('session_start', 'trade_complete', 'in_game_spend')
  AND to_date(timestamp) >='2023-05-01'
),

purchasers AS (
SELECT *
FROM hive_metastore.gold_bi.store_revenue
WHERE status = 'fulfilled'
  AND total_price_usd > 0
  AND game NOT IN ('bridge','swap')
),

no_bots AS (
  SELECT DISTINCT a.user_id AS user_id
  FROM loads a
  LEFT JOIN dev.anna_temp.mayhem_selected_multiacc b ON a.user_id = b.user_id
  LEFT JOIN purchasers c ON a.user_id = c.userid AND b.user_id IS NOT NULL
  WHERE b.user_id IS NULL OR c.userid IS NOT NULL
),

user_details AS (
  SELECT a.user_id, b.referred_by, b.discord_id
  FROM no_bots a
  LEFT JOIN prd.gala_blockchaingamepartners.slv_users b ON a.user_id = b.user_id
),

top_1200 AS (
  SELECT user_id, COUNT(user_id) AS top_1200
  FROM hive_metastore.prd_slv_townstar_events.rankings
  GROUP BY user_id
),

combined AS (
  SELECT a.user_id, b.referred_by, b.discord_id, e.top_1200,
    CASE WHEN top_1200 >= 1 THEN 1500 END AS gala_4,
    CASE WHEN top_1200 >= 1 AND referred_by != '' THEN 375 END AS gala_4_r
  FROM no_bots a
  LEFT JOIN user_details b ON a.user_id = b.user_id
  LEFT JOIN top_1200 e ON a.user_id = e.user_id
)

SELECT distinct user_id
FROM combined where gala_4 = 1500 and user_id not in
(SELECT distinct user_id from hive_metastore.prd_slv_townstar_events.rankings where comp_name = 'Shrimp Pizza')
and user_id not in (SELECT distinct user_id from hive_metastore.prd_slv_townstar_events.achievements where id = 'achievement_4');