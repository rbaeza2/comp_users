WITH loads AS (
  SELECT DISTINCT user_id
  FROM prd.game_telemetry.brz_landing
  WHERE game = 'town-star-forever'
  AND event_type IN ('session_start', 'trade_complete', 'in_game_spend')
  AND to_date(timestamp) >='2023-05-01'
),

purchasers AS (
SELECT *
FROM hive_metastore.gold_bi.store_revenue
WHERE status = 'fulfilled'
  AND total_price_usd > 0
  AND game NOT IN ('bridge','swap')
),

no_bots AS (
  SELECT DISTINCT a.user_id AS user_id
  FROM loads a
  LEFT JOIN dev.anna_temp.mayhem_selected_multiacc b ON a.user_id = b.user_id
  LEFT JOIN purchasers c ON a.user_id = c.userid AND b.user_id IS NOT NULL
  WHERE b.user_id IS NULL OR c.userid IS NOT NULL
),

user_details AS (
  SELECT a.user_id, b.referred_by, b.discord_id
  FROM no_bots a
  LEFT JOIN prd.gala_blockchaingamepartners.slv_users b ON a.user_id = b.user_id
),

top_1200 AS (
  SELECT user_id, COUNT(user_id) AS t1200
  FROM hive_metastore.prd_slv_townstar_events.rankings
  GROUP BY user_id
),

top_800 AS (
  SELECT user_id, COUNT(user_id) AS t800
  FROM hive_metastore.prd_slv_townstar_events.rankings
  where final_rank <= 800
  GROUP BY user_id
),

top_500 AS (
  SELECT user_id, COUNT(user_id) AS t500
  FROM hive_metastore.prd_slv_townstar_events.rankings
  where final_rank <= 500
  GROUP BY user_id
),

combined AS (
  SELECT a.user_id, top_1200.t1200, top_800.t800, top_500.t500
  FROM no_bots a
  LEFT JOIN user_details b ON a.user_id = b.user_id
  LEFT JOIN top_1200 ON a.user_id = top_1200.user_id
  LEFT JOIN top_800 on a.user_id = top_800.user_id
  LEFT JOIN top_500 on a.user_id = top_500.user_id
)

SELECT distinct user_id
FROM combined where t1200 >= 7 and t800 >= 1
and user_id not in (SELECT distinct user_id from hive_metastore.prd_slv_townstar_events.achievements where id = 'achievement_5');