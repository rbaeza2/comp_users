import sys
sys.path.append('./')
from common.helper import Helper

class SQLHelper:

	# not working, for some reason cannot access spart in another file
	def insert(spark, table, columns, values):
		df = spark.createDataFrame(values, schema = columns)
		return df.write.insertInto(table)


	def GetQuery(queryName):
		try:
			with open(Helper.GetFilePathInMyDir(__file__, queryName + ".sql"), 'r') as f:
				return f.read()
		except Exception as error:
			print("sql/helper: GetQuery: ", error)
			return ""

