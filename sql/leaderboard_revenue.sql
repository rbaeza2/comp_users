SELECT DISTINCT leaderboard.user_id as user_id,
                rev.revenue as revenue
FROM
  (SELECT DISTINCT userid AS user_id,
                   sum(total_price_usd) AS revenue
   FROM gold_bi.store_revenue
   WHERE game = 'Town Star'
     AND refunded = 'false'
     AND status = 'fulfilled'
   GROUP BY userid) rev
INNER JOIN
  (SELECT user_id
   FROM hive_metastore.prd_slv_townstar_events.leaderboard
   WHERE comp_name = '{compName}') leaderboard ON rev.user_id = leaderboard.user_id