SELECT user_id,
       first_name,
       last_name,
       email,
       created,
       un_verified_ip,
       user_ip_address,
       verified_ips
FROM prd.gala_blockchaingamepartners.slv_users
WHERE user_id IN
    (SELECT DISTINCT user_id
     FROM hive_metastore.prd_slv_townstar_events.leaderboard
     WHERE comp_name = '{compName}');