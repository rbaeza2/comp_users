SELECT DISTINCT rankings.user_id
FROM
  (SELECT DISTINCT user_id
   FROM hive_metastore.prd_slv_townstar_events.game_launch
   WHERE to_date(from_unixtime(TIMESTAMP / 1000)) BETWEEN '{year}-04-30' AND '{year}-06-01') launch
INNER JOIN
  (SELECT user_id
   FROM hive_metastore.prd_slv_townstar_events.rankings
   WHERE comp_name = '{compName}') rankings ON launch.user_id = rankings.user_id;
