SELECT DISTINCT rankings.user_id
FROM
  (SELECT DISTINCT user_id
   FROM prd.gala_blockchaingamepartners.slv_users
   WHERE to_date(created) < '2021-06-01') launch
INNER JOIN
  (SELECT user_id
   FROM hive_metastore.prd_slv_townstar_events.rankings
   WHERE comp_name = '{compName}') rankings ON launch.user_id = rankings.user_id