WITH 
meta_trades_users AS (
  SELECT t1.user_id 
  FROM prd.game_telemetry.brz_landing t1
  JOIN prd.game_telemetry.brz_landing t2 ON get_json_object(t1.event_detail, '$.townId') = get_json_object(t2.event_detail, '$.id')
  WHERE t1.game = 'town-star-forever'
    AND t1.event_type = 'trade_complete'
    AND get_json_object(t1.event_detail, '$.item') in ('{meta_item}')
    AND t2.event_type = 'create_board'
    AND get_json_object(t2.event_detail, '$.competition_id') = '{comp_id}'
    AND t1.source_timestamp >= '{start_time}' 
    AND t1.source_timestamp <= '{end_time}'
  GROUP BY t1.user_id
),

parti AS (
  SELECT *
  FROM hive_metastore.prd_slv_townstar_events.leaderboard 
  WHERE comp_name = '{comp_name}'
),

top_1200 AS (
  SELECT user_id, final_rank
  FROM hive_metastore.prd_slv_townstar_events.rankings
  WHERE comp_name = '{comp_name}'
),

reward_list AS (SELECT l.user_id, l.stars, l.rank, t.final_rank,
       CASE WHEN m.user_id IS NULL THEN 0 ELSE 1 END AS meta_trade,
       CASE WHEN f.user_id IS NOT NULL THEN 1 ELSE 0 END AS dupe
FROM parti l
LEFT JOIN meta_trades_users m ON l.user_id = m.user_id
LEFT JOIN dev.anna_temp.mayhem_selected_multiacc f ON l.user_id = f.user_id
LEFT JOIN top_1200 t ON l.user_id = t.user_id
GROUP BY l.user_id, l.stars, l.rank, t.final_rank, meta_trade, dupe
),

clean_dist_list AS (
  SELECT user_id
  FROM reward_list
  WHERE final_rank IS NULL
  AND meta_trade = 1
)

SELECT user_id
FROM clean_dist_list
;