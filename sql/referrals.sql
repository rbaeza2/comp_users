SELECT refferred.user_id AS user_id,
       affiliates.user_id AS referrer
FROM
  (SELECT user_id,
          referred_by
   FROM prd.gala_blockchaingamepartners.slv_users) refferred
INNER JOIN
  (SELECT user_id,
          affiliate_id
   FROM prd.gala_blockchaingamepartners.slv_users) affiliates ON refferred.referred_by = affiliates.affiliate_id;