 WITH firstSession AS
  (SELECT user_id,
          min(date) AS firstLogin
   FROM (
           (SELECT user_id,
                   min(to_date(from_unixtime(TIMESTAMP/1000))) AS date
            FROM prd_slv_townstar_events.game_launch
            GROUP BY user_id)
         UNION
           (SELECT user_id,
                   min(to_date(source_timestamp)) AS date
            FROM prd.game_telemetry.brz_landing
            WHERE game = 'town-star-forever'
              AND event_type = 'session_start'
            GROUP BY user_id))
   GROUP BY user_id)
INSERT INTO hive_metastore.prd_slv_townstar_events.resurrection
SELECT distinct(user_id),
       to_date(source_timestamp),
       'town-star-forever'
FROM prd.game_telemetry.brz_landing
WHERE game = 'town-star-forever'
  AND event_type = 'session_start'
  AND to_date(source_timestamp) = '{day}'
  AND user_id NOT IN
    (SELECT user_id
     FROM firstSession
     WHERE firstLogin = '{day}')
  AND user_id NOT IN (
                        (SELECT distinct(user_id)
                         FROM hive_metastore.prd_slv_townstar_events.game_launch
                         WHERE to_date(from_unixtime(TIMESTAMP/1000)) BETWEEN date_sub('{day}', 31) AND date_sub('{day}', 1))
                      UNION
                        (SELECT distinct(user_id)
                         FROM prd.game_telemetry.brz_landing
                         WHERE event_type = 'session_start'
                           AND game = 'town-star-forever'
                           AND to_date(source_timestamp) BETWEEN date_sub('{day}', 31) AND date_sub('{day}', 1)))